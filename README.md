# Active shelf monitoring for retail
Camera that is able to count inventory on the shelf and report to cloud on real time basis

# Project Status 
As of December 2020, 6 models have been implemented and tested on MVTec D2S Dataset. The code, detailed reports of each of the models accuracy, issues faced while implementing them are placed in each model's folder. The Inventory management tasks are remaining

## Ways you can contribute 
You are welcome to contribute to this project. Ways you can help:
* Project has been divided into sub-tasks and added in the [Issues](https://gitlab.com/iotiotdotin/project-internship-ai/products/active-shelf-monitoring-for-retail/-/issues)  section of this gitlab project. You can choose any issue in the domain you are familiar with and contribute 
* Feel like this project could use a new feature, make an issue with details to discuss how it can be implemented and work on it
* Find a bug and report it.
* Help fix a reported bug.
* Help with documenting the whole project.
* Write a How-to Guide.

## How to work on an issue: 
* Clone the repo to your system.
* Copy files to the respective directory
* Create a Merge request
* Maintainer reviews the code and merges

## Architecture :
<img src="projectArchitechture.jpg" alt="Project Architechture" width =700>

## Communication :
Join the [Discord Server](https://discord.gg/mXgGsaj) for discussions

## Project Maintainer : 
[Gautam Jagdhish](https://gitlab.com/gautamjagdhish)  

## Contributors
### Models :
* YOLOv4 - [Gautam Jagdhish](https://gitlab.com/gautamjagdhish)
* EfficientDet - [Sam Joy](https://gitlab.com/samj124)
* Faster_RCNN_Inception_v2 - [Rishav Dash](https://github.com/Rishav-hub)
* Faster_RCNN_ResNeXt - [Gugan Kathiresan](https://gitlab.com/GuganK)
* SSD_MobileNet_v2 - []()
* YOLO_v5 - []()
